package main.es.alejandro;

import java.net.*;
import java.io.*;
import javax.swing.*;

public class Servidor {
	
	public static void main(String[] args) {
		MarcoServidor mimarco = new MarcoServidor();
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}


class MarcoServidor extends JFrame implements Runnable{
	private JTextArea areatexto;
    private JScrollPane scrollpanel;

	public MarcoServidor() {
		super("Ventana servidor");
		setBounds(1200,300,280,350);
		
		areatexto = new JTextArea(20, 20);
		scrollpanel = new JScrollPane(areatexto);
        scrollpanel.setBounds(10, 50, 400, 300);
        add(scrollpanel);
        areatexto.setEditable(false);
		
		setVisible(true);
		Thread hilo = new Thread(this);
		hilo.start();
	}
	
	@Override
	public void run() {
		try {
			ServerSocket servidor = new ServerSocket(9999); // Abrimos una conexi�n a trav�s del puerto, en este caso 9999
			while(true) {
				Socket socket = servidor.accept(); //Acceptamos todas las conexiones
				DataInputStream flujoEntrada = new DataInputStream(socket.getInputStream()); //Leemos lo que viene en el flujo de datos
				String mensajeTexto = flujoEntrada.readUTF(); //Guardamos el flujo de datos del cliente
				areatexto.append("\n" + mensajeTexto); //Imprimimos el texto del cliente en el area de texto
				socket.close(); //cerramos la conexi�n
				//servidor.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
	