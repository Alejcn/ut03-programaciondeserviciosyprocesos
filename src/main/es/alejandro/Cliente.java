package main.es.alejandro;

import java.net.*;
import java.io.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Cliente {
	public static String nickUsuario;
	public static void main(String[] args) {
		nickUsuario = JOptionPane.showInputDialog("Introduce tu nickname: ");
		MarcoCliente mimarco = new MarcoCliente();
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}	
}

class MarcoCliente extends JFrame {
	public MarcoCliente() {
		super("Ventana cliente: " + Cliente.nickUsuario);
		setBounds(600,300,280,350);
		LaminaMarcoCliente milamina = new LaminaMarcoCliente();
		add(milamina);
		setVisible(true);	
		
	}
}

class LaminaMarcoCliente extends JPanel {
	private JTextField campo1;
	private JButton botonEnviar;
	public static JTextArea areatexto;
    private JScrollPane scrollpanel;

	public LaminaMarcoCliente() {
		JLabel nick = new JLabel("Nick: " + Cliente.nickUsuario);
		add(nick);
		
		campo1 = new JTextField(20);
		add(campo1);
		
		botonEnviar = new JButton("Enviar");
		EnviarTexto mievento = new EnviarTexto();
		botonEnviar.addActionListener(mievento);
		add(botonEnviar);
		
		areatexto = new JTextArea(20, 20);
		scrollpanel = new JScrollPane(areatexto);
        scrollpanel.setBounds(10, 50, 400, 300);
        add(scrollpanel);
        areatexto.setEditable(false);
	}
	
	private class EnviarTexto implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				Socket socket = new Socket("localhost", 9999); //Se construye el socket
				DataOutputStream flujoSalida = new DataOutputStream(socket.getOutputStream()); //Se crea el flujo
				flujoSalida.writeUTF(Cliente.nickUsuario + ": "+ campo1.getText()); //Mete dentro del flujo lo que escribamos
				LaminaMarcoCliente.areatexto.append("\n" + campo1.getText());
				flujoSalida.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		
	}
}
